/**
 *

 */

var weeks = new Array('日','月','火','水','木','金','土');

var now = new Date();

var year = now.getYear();
var month = now.getMonth() + 1;
var day = now.getDate();
var week = weeks[ now.getDay() ];
var hour = now.getHours();
var minute = now.getMinutes();
var second = now.getSeconds();

if(year < 2000) { year += 1900;}

//数字が一桁の時に頭に０をつけて二桁で表示する
if(month < 10) { month = "0" + month; }
if(day < 0) { day = "0" + day; }
if(hour < 0) { hour = "0" + hour; }
if(minute < 10) { minute = "0" + minute; }
if(second < 10) { second = "0" + second; }

document.write('現在：<b>' + year + '年' + month + '月' + day + '日（' + week + '）');
document.write(hour + '時' + minute + '分' + second + '秒</b>');