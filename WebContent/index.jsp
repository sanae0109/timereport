<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>ログイン</title>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/Login.css">
<link rel="stylesheet" href="css/CommonHeader.css">
<link rel="stylesheet" href="css/CommonFooter.css">
<link rel="stylesheet" href="css/TimeReport.css">


<!-- header -->
<header>
	<div class="header-inner">
		<img class="header-logo" src="picture/logo.png?text=LOGO">
		<div class="header-title">勤怠管理システム</div>
		<div class="header-navgroup">

		</div>
	</div>
</header>

</head>
<body>


	<div class="container">
		<div class="card mx-auto border-dark mb-3" id="naito">
			<div class="card-header">
				<b>ログイン</b>
			</div>
			<div class="card-body">
				<form action="LoginServlet" method="post">
					<p class="login-card">
						メールアドレス : <input type="email" name="mail_address">
					<p>
					<p class="login-card">
						   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; パスワード : <input type="password" name="password">
					<p>
					<p class="login-card">
						<input type="submit" value="ログイン" class="button">
					</p>
				</form>

				<p class="sign-up">
				<form action="MyAccountInsertForwardServlet" method="post">
					<input type="submit" value="新規アカウントの登録" class="button">
				</form>
				</p>
			</div>
		</div>
	</div>

	<!-- footer -->
	<footer>
		<p>© 2020（有限会社）介護福太郎</p>
	</footer>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>