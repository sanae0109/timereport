<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, dto.UserInfo"%>

<%@ page import="java.util.*, dto.Authority"%>


<%
	String userId = (String) request.getAttribute("userInfo.getUserId()");

	UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");

	Authority authority = (Authority) session.getAttribute("authority");

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>勤怠管理システム</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/CommonHeader.css">
<link rel="stylesheet" href="css/CommonFooter.css">
<link rel="stylesheet" href="css/Attendance.css">

</head>
<body>

	<div class="wrapper">


		<!-- header -->
		<header>
			<div class="header-inner">

				<form name="form_1" id="form_1" action="data/post">
					<input type="hidden" name="logo" placeholder="ロゴマーク"> <a
						href="AttendanceForwardServlet"><img class="header-logo"
						src="picture/logo.png?text=LOGO"></a>
				</form>

				<div class="header-title">勤怠管理システム</div>
				<div class="header-navgroup">

					<div class="header-navitem">
						ログイン者名：<%=userInfo.getUserName()%></div>

				</div>
			</div>
		</header>

		<div class="container">

			<div class="column">


				<h3>
					<%=userInfo.getUserName()%>さんお疲れ様です。
				</h3>

			</div>
			<div class="column">


				<h5>
					<script type="text/javascript" src="js/nowTime.js"></script>
				</h5>
			</div>

			<br>

			<div class="column">

				<div class="admission">
					<!-- 出社 -->
					<form action="AdmissionServlet" method="post" class="btn1">
						<input type="hidden" name="userInfo.getUserId()"
							value="<%=userInfo.getUserId()%>"> <input type="submit"
							name="submit" value="出社" class="button1">
					</form>
				</div>
				<div class="leaving">
					<!-- 退社 -->
					<form action="LeavingServlet" method="post">
						<input type="hidden" name="userInfo.getUserId()"
							value="<%=userInfo.getUserId()%>"> <input type="submit"
							name="submit" value="退社" class="button2">
					</form>
				</div>

			</div>

			<div class="column">

				<div class="attendance">
					<!-- 勤務管理 -->
					<form action="TimeReportServlet" method="post" class="btn3">
						<input type="hidden" name="userInfo.getUserId()"
							value="<%=userInfo.getUserId()%>"> <input type="submit"
							name="submit" value="勤務管理" class="button">
					</form>
				</div>

			</div>
			<div class="column">

				<!-- マイアカウント更新 -->
				<form action="MyAccountUpdateForwardServlet" method="post">
					<input type="submit" name="submit" value="社員情報更新" class="button">
				</form>
			</div>


<%if(authority != null)  {%>

<div class="column">
				<!-- 管理者専用画面に戻る -->
				<form action="AuthorityForwardServlet" method="post">
					<input type="submit" name="submit" value="管理者専用画面に戻る" class="button">
				</form>
			</div>
<%} %>

			<div class="column">

				<div class="logout" style="text-align: right;">


					<a href="LogoutServlet" class="btn btn--yellow btn--cubic">ログアウト</a>

				</div>
			</div>


		</div>

		<!-- footer -->
		<footer>
			<p>© 2020（有限会社）介護福太郎</p>
		</footer>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>