<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, dto.UserInfo"%>

<%
	String userId = (String) request.getAttribute("userInfo.getUserId()");

	UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者専用画面です</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/CommonHeader.css">
<link rel="stylesheet" href="css/CommonFooter.css">
<link rel="stylesheet" href="css/TimeReport.css">
<link rel="stylesheet" href="css/Authority.css">

</head>
<body>

	<div class="wrapper">


		<!-- header -->
		<header>
			<div class="header-inner">
				<form name="form_1" id="form_1" action="data/post">
					<input type="hidden" name="logo" placeholder="ロゴマーク"> <a
						href="AttendanceForwardServlet"><img class="header-logo"
						src="picture/logo.png?text=LOGO"></a>
				</form>
				<div class="header-title">勤怠管理システム</div>
				<div class="header-navgroup">

					<div class="header-navitem">
						ログイン者名：<%=userInfo.getUserName()%></div>

				</div>
			</div>
		</header>

		<div class="column">

			<h1>管理者専用画面</h1>

			<p>
			<h4><%=userInfo.getUserName()%>さんお疲れ様です。
			</h4>
			</p>
		</div>

		<div class="column">

			<h4>
				<script type="text/javascript" src="js/nowTime.js"></script>
			</h4>
		</div>


		<div class="column">

				<font size="5">自身の勤怠管理が行えます</font>

			<!-- 勤怠管理メニュー画面 -->
			<form action="AttendanceForwardServlet" method="get">
				<input type="submit" name="submit" value="勤怠管理メニュー" class="button1">
			</form>
		</div>

		<br>

		<div class="column">
				<font size="5"> 社員の勤務管理表を参照できます </font>


			<!-- 社員の勤怠管理表を参照 -->
			<form action="AuthorityTimeReportManagementForwardServlet"
				method="post">
				<input type="submit" name="submit" value="社員の勤怠管理表を参照"
					class="button1">
			</form>
		</div>

<%--

		<div class="column">

			<!-- 社員情報、部署の管理 -->
			<form action="AuthorityAccountAndDepartmentForwardServlet"
				method="post">
				<input type="submit" name="submit" value="社員情報、部署の管理"
					class="button1">
			</form>
		</div>

		  --%>

		<div class="column">


			<div class="logout" style="text-align: right;">


				<a href="LogoutServlet" class="btn btn--yellow btn--cubic">ログアウト</a>

			</div>
		</div>


	</div>
	<!-- footer -->
	<footer>
		<p>© 2020（有限会社）介護福太郎</p>
	</footer>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

</body>
</html>