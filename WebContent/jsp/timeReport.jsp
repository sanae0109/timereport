<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.*, dto.TimeReport"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.*, dto.UserInfo"%>


<%
	String userId = (String) request.getAttribute("userInfo.getUserId()");
	UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");

	ArrayList<TimeReport> timeList = (ArrayList<TimeReport>) request.getAttribute("timeReport");
	ArrayList<TimeReport> timeList2 = (ArrayList<TimeReport>) request.getAttribute("timeReport2");
	Object totalActualTime = request.getAttribute("totalActualTime");
	Object totalOverTime = request.getAttribute("totalOverTime");
	Object totalWorkDays = request.getAttribute("totalWorkDays");
	Object errorMassage = request.getAttribute("errorMassage");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>勤務管理</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/CommonHeader.css">
<link rel="stylesheet" href="css/CommonFooter.css">
<link rel="stylesheet" href="css/TimeReport.css">


</head>
<body>

	<div class="wrapper">

		<!-- header -->
		<header>
			<div class="header-inner">
				<form name="form_1" id="form_1" action="data/post">
					<input type="hidden" name="logo" placeholder="ロゴマーク"> <a
						href="AttendanceForwardServlet"><img class="header-logo"
						src="picture/logo.png?text=LOGO"></a>
				</form>
				<div class="header-title">勤怠管理システム</div>
				<div class="header-navgroup">

					<div class="header-navitem">
						ログイン者名：<%=userInfo.getUserName()%></div>

				</div>
			</div>
		</header>
		<div class="main">
			<h1>勤務管理画面</h1>

			月を選択してください

			<p>
				<font color="red"> <%
 	if (errorMassage == null) {
 %> <%
 	} else {
 %> <%=errorMassage%> <%
 	}
 %>
				</font>
			</p>

			<form action="GetWorkDays" method="post">
				<input type="month" name="month"> <input type="submit"
					value="月切替">
			</form>
			<br>

			<%
				if (timeList2 != null) {
			%>


			<table>
				<tr>
					<td>合計残業時間</td>

					<td>合計作業時間</td>

					<td>出勤日数</td>

				</tr>

				<tr>
					<td>
						<%
							if (totalOverTime != null) {
						%> <%=totalOverTime%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>

					<td>
						<%
							if (totalActualTime != null) {
						%> <%=totalActualTime%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>
					<td>
						<%
							if (totalWorkDays != null) {
						%> <%=totalWorkDays%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>
				</tr>

			</table>
			<br>








			<table>


				<tr>
					<td>出社日</td>
					<td class="timeReportTable">曜日</td>
					<td class="timeReportTable">出社時刻</td>
					<td>退社時刻</td>
					<td>休憩時間</td>
					<td>残業時間</td>
					<td>作業時間</td>
					<td>作業場所</td>
					<td>作業内容</td>

				</tr>



				<%
					if (timeList2 != null) {
							for (int i = 0; i < timeList2.size(); i++) {
				%>
				<tr>

					<td><%=timeList2.get(i).getWorkingDate()%></td>

					<td>
						<%
							if (timeList2.get(i).getDayOfWeek() != null) {
						%> <%=timeList2.get(i).getDayOfWeek()%> <%
 	} else {
 %> <%
 	}
 %>
					</td>


					<td>
						<%
							if (timeList2.get(i).getStartTime() != null) {
						%> <%=timeList2.get(i).getStartTime()%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>

					<td>
						<%
							if (timeList2.get(i).getFinishTime() != null) {
						%> <%=timeList2.get(i).getFinishTime()%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>
					<td>
						<%
							if (timeList2.get(i).getBreakTime() != null) {
						%> <%=timeList2.get(i).getBreakTime()%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>
					<td>
						<%
							if (timeList2.get(i).getOverTime() != null) {
						%> <%=timeList2.get(i).getOverTime()%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>
					<td>
						<%
							if (timeList2.get(i).getActualTime() != null) {
						%> <%=timeList2.get(i).getActualTime()%> <%
 	} else {
 %> --:-- <%
 	}
 %>
					</td>
					<td>
						<%
							if (timeList2.get(i).getLocation() != null) {
						%> <%=timeList2.get(i).getLocation()%> <%
 	} else {
 %> -- <%
 	}
 %>
					</td>
					<td>
						<%
							if (timeList2.get(i).getContent() != null) {
						%> <%=timeList2.get(i).getContent()%> <%
 	} else {
 %> -- <%
 	}
 %>
					</td>



				</tr>
				<%
					}
						}
				%>

			</table>
			<%
				}
			%>

		</div>

		<!-- footer -->
		<footer>
			<p>© 2020（有限会社）介護福太郎</p>
		</footer>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

</body>
</html>