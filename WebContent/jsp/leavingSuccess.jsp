<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@ page import="java.util.*, dto.UserInfo"%>

<%
	String userId = (String) request.getAttribute("userInfo.getUserId()");

	UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>退社完了</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/CommonHeader.css">
<link rel="stylesheet" href="css/CommonFooter.css">
<link rel="stylesheet" href="css/TimeReport.css">
</head>
<body>
	<div class="wrapper">


		<!-- header -->
		<header>
			<div class="header-inner">
				<form name="form_1" id="form_1" action="data/post">
					<input type="hidden" name="logo" placeholder="ロゴマーク"> <a
						href="AttendanceForwardServlet"><img class="header-logo"
						src="picture/logo.png?text=LOGO"></a>
				</form>
				<div class="header-title">勤怠管理システム</div>
				<div class="header-navgroup">

					<div class="header-navitem">
						ログイン者名：<%=userInfo.getUserName()%></div>

				</div>
			</div>
		</header>

		<div class="column">
			<h1>退社完了しました、本日の業務内容を登録してください</h1>
		</div>

		<div class="column">

			<form action="WorkContentInsert" method="post">
				<p>
					作業場所 : <br> <input type="text" name="location">
				</p>
				<p>
					作業内容 : <br> <input type="text" name="content">
				</p>
				<input type="submit" value="業務内容を登録する" id="button2" class="button">
			</form>
		</div>

		<!-- footer -->
		<footer>
			<p>© 2020（有限会社）介護福太郎</p>
		</footer>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

</body>
</html>