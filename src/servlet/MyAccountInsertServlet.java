package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MyAccountInsertDao;
import dto.UserInfo;

@WebServlet(name = "MyAccountInsertServlet", urlPatterns = { "/MyAccountInsertServlet" })

public class MyAccountInsertServlet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String forwardPath = "";
		request.setCharacterEncoding("UTF-8");

		String userName = request.getParameter("userName");
		String mailAddress = request.getParameter("mailAddress");
		String password = request.getParameter("password");

		UserInfo userInfo = new UserInfo(userName, mailAddress, password);

		try {

			MyAccountInsertDao myAccountInsertDao = new MyAccountInsertDao();

			myAccountInsertDao.myAccountInsert(userInfo);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();

		}

		forwardPath = "jsp/myAccountInsertSuccess.jsp";

		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}
}
