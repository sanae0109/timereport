package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MyAccountUpdateDao;
import dto.UserInfo;

@WebServlet(name = "MyAccountUpdateServlet", urlPatterns = { "/MyAccountUpdateServlet" })

public class MyAccountUpdateServlet extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String forwardPath = "";
		request.setCharacterEncoding("UTF-8");

		String userName = request.getParameter("userName");
		String mailAddress = request.getParameter("mailAddress");
		String password = request.getParameter("password");

		HttpSession session = request.getSession();

		UserInfo userInfo = (UserInfo)session.getAttribute("userInfo");


		try {

			MyAccountUpdateDao myAccountUpdateDao = new MyAccountUpdateDao();

			myAccountUpdateDao.myAccountUpdate(userInfo,userName,mailAddress,password);



		} catch (SQLException | ClassNotFoundException  e) {
			e.printStackTrace();

		}

		forwardPath = "jsp/myAccountUpdateSuccess.jsp";

		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);


	}

}
