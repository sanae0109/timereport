package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.WorkContentInsertDao;
import dto.TimeReport;

@WebServlet(name = "WorkContentInsert", urlPatterns = { "/WorkContentInsert" })
public class WorkContentInsertServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String forwardPath = "";
		forwardPath = "jsp/timeReport.jsp";

		HttpSession session = request.getSession();


		String userId = (String) session.getAttribute("userInfo.getUserId()");


		try {

			String location = request.getParameter("location");
			String content = request.getParameter("content");

			TimeReport timeReport = new TimeReport(location, content);


			WorkContentInsertDao workContentInsertDao = new WorkContentInsertDao();

			workContentInsertDao.workContentInsert(timeReport, userId);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();

		}

		forwardPath = "jsp/workContentInsertSuccess.jsp";

		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}
}
