package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AuthorityDepartmentListForwardServlet", urlPatterns = { "/AuthorityDepartmentListForwardServlet" })

public class AuthorityDepartmentListForwardServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String forwardPath = "";
	forwardPath = "jsp/authorityDepartmentList.jsp";
	RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
	dispatcher.forward(request, response);

	}

}
