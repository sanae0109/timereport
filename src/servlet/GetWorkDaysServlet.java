package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.GetWorkDaysDao;
import dao.TotalActualTimeDao;
import dao.TotalOverTimeDao;
import dao.TotalWorkDaysDao;
import dto.TimeReport;

@WebServlet(name = "GetWorkDays", urlPatterns = { "/GetWorkDays" })
public class GetWorkDaysServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String forwardPath = "";
		forwardPath = "jsp/timeReport.jsp";

		HttpSession session = request.getSession();

		String userId = (String) session.getAttribute("userInfo.getUserId()");

		String month = request.getParameter("month");

		if (month.equals("")) {
			String errorMassage = "月が選択されませんでした";
			request.setAttribute("errorMassage", errorMassage);

			RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
			dispatcher.forward(request, response);

		}

		String[] yearMonth = month.split(",", -1);

		int i = buildYear(month);

		int a = buildMonth(month);

		int b = getMaximumDays(i, a);

		List<TimeReport> timeList2;

		try {

			timeList2 = GetWorkDaysDao.GetWorkDays(userId, b, i, a);

			String totalActualTime = TotalActualTimeDao.TotalActualTime(timeList2);

			String totalOverTime = TotalOverTimeDao.TotalOverTime(timeList2);

			int totalWorkDays = TotalWorkDaysDao.TotalWorkDays(timeList2);

			for (int d = 0; d < timeList2.size(); d++) {

			}

			request.setAttribute("totalWorkDays", totalWorkDays);

			request.setAttribute("totalOverTime", totalOverTime);

			request.setAttribute("totalActualTime", totalActualTime);

			request.setAttribute("timeReport2", timeList2);

			RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
			dispatcher.forward(request, response);

		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	//年を分けるメソッド
	public int buildYear(String nen) {
		StringBuilder buildYear = new StringBuilder();

		System.out.println(nen);
		String[] monthArray = nen.split("");
		for (int i = 0; i < 4; i++) {
			buildYear.append(monthArray[i]);
		}
		nen = new String(buildYear);
		System.out.println(nen);
		int year = Integer.parseInt(nen);
		return year;
	}

	//月を分けるメソッド
	public int buildMonth(String tuki) {
		StringBuilder buildMonth = new StringBuilder();
		String[] monthArray = tuki.split("");
		for (int i = 5; i < 7; i++) {
			buildMonth.append(monthArray[i]);
		}
		tuki = new String(buildMonth);
		int month = Integer.parseInt(tuki);
		return month;
	}

	//日を分けるメソッド
	public int buildDay(String hi) {
		StringBuilder buildDay = new StringBuilder();
		String[] dayArray = hi.split("");
		for (int i = 8; i < 10; i++) {
			buildDay.append(dayArray[i]);
		}
		hi = new String(buildDay);
		int day = Integer.parseInt(hi);
		return day;
	}

	//月の最終日取得メソッド
	public int getMaximumDays(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		int result = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return result;
	}

}
