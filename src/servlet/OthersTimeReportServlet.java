package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "OthersTimeReportServlet", urlPatterns = { "/OthersTimeReportServlet" })

public class OthersTimeReportServlet extends HttpServlet{
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		String otherId = (String) request.getParameter("name");
		session.setAttribute("otherId",otherId );
		System.out.println(otherId);

		String forwardPath = "";
		forwardPath = "jsp/otherTimeReport.jsp";

		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}


}
