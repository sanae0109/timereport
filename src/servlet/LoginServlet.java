package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountLoginDao;
import dao.AuthorityLoginDao;
import dto.Authority;
import dto.Login;
import dto.UserInfo;

@WebServlet(name = "LoginServlet", urlPatterns = { "/LoginServlet" })
public class LoginServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String mailAddress = request.getParameter("mail_address");


		System.out.println(password);
		System.out.println(mailAddress);

		String forwardPath = "";

		//ログイン処理
		try {

			Login login = new Login(mailAddress,password);
			AccountLoginDao accountLoginDao = new AccountLoginDao();
			UserInfo userInfo = accountLoginDao.Login(login);



			if(userInfo == null) {
				System.out.println("うんちうまーーーーい");
				forwardPath = "/index.jsp";

			}else if(userInfo != null){

				AuthorityLoginDao authorityLoginDao = new AuthorityLoginDao();
				Authority authority = authorityLoginDao.AuthorityLogin(userInfo.getUserName());


				HttpSession session = request.getSession();

				session.setAttribute("userInfo.getUserName()",userInfo.getUserName());

                session.setAttribute("userInfo.getMailAddress()",userInfo.getMailAddress());
                session.setAttribute("userInfo.getPassword()", userInfo.getPassword());
				session.setAttribute("userInfo.getUserId()",userInfo.getUserId());
				session.setAttribute("userInfo", userInfo);

				forwardPath = "jsp/attendance.jsp";

				if(authority != null) {
					System.out.println("管理者ログイン");
					System.out.println(authority);
					forwardPath = "jsp/authority.jsp";
					session.setAttribute("authority", authority);


				}
			}



		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println("例外で");
			forwardPath = "/index.jsp";
		}



		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);




	}
}
