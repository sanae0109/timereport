package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AuthorityAccountAndDepartmentForwardServlet", urlPatterns = { "/AuthorityAccountAndDepartmentForwardServlet" })

public class AuthorityAccountAndDepartmentForwardServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String forwardPath = "";
	forwardPath = "jsp/authorityAccountAndDepartment.jsp";
	RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
	dispatcher.forward(request, response);

	}
}
