package servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.Admission;
import dto.UserInfo;

@WebServlet(name = "AdmissionServlet", urlPatterns = { "/AdmissionServlet" })
public class AdmissionServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userId = request.getParameter("userInfo.getUserId()");
		System.out.println(userId);
		request.setCharacterEncoding("UTF-8");

		UserInfo userInfo = new UserInfo();

		Date today = new Date(System.currentTimeMillis());

		System.out.println(today);


		String forwardPath = "";

		try {
			Admission Admission = new Admission();
			boolean canAdmission = Admission.isExecute(userId,today);

			// 出社できれば画面遷移します
			if (canAdmission) {
				request.setAttribute("userInfo.getUserId()",userInfo.getUserId());
				forwardPath = "jsp/admissionSuccess.jsp";

			// できなければエラーメッセージ出力
			} else {
				System.out.println("本日はすでに出社済みです");
				forwardPath = "jsp/admissionFault.jsp";
			}

		// 例外処理
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println("システムエラーが発生しました。管理者にご連絡ください");
			forwardPath = "jsp/attendance.jsp";
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		// 二度目以降もuserIdに値が入るように保存
		request.setAttribute("userInfo.getUserId()",userInfo.getUserId());

		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}

}
