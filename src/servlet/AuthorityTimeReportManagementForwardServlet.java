package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AllAcountSelectDao;
import dto.UserInfo;

@WebServlet(name = "AuthorityTimeReportManagementForwardServlet", urlPatterns = {
		"/AuthorityTimeReportManagementForwardServlet" })

public class AuthorityTimeReportManagementForwardServlet extends HttpServlet{
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String forwardPath = "";
		forwardPath = "jsp/authorityTimeReportManagement.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String forwardPath = "";



		try {
			List<UserInfo> accountList = AllAcountSelectDao.AllAcountSelect();
			System.out.println(accountList.get(1).getUserName());

			request.setAttribute("accountList", accountList);

		} catch (ClassNotFoundException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
















		forwardPath = "jsp/authorityTimeReportManagement.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}

}
