package dao;

import java.sql.SQLException;
import java.util.List;

import dto.TimeReport;

public class TotalActualTimeDao {

	public static String TotalActualTime(List<TimeReport> timeList2) throws ClassNotFoundException, SQLException {

		String totalActualTime = null;
		int sumHour = 0;
		int sumMinute = 0;

		for (int i = 0; i < timeList2.size(); i++) {
			if (timeList2.get(i).getActualTime() != null) {
				String stringActualTime = String.valueOf(timeList2.get(i).getActualTime());
				System.out.println(stringActualTime);
				int hour = buildHour(stringActualTime);
				int minute = buildMinute(stringActualTime);
				sumHour += hour;
				sumMinute += minute;
				if (sumMinute >= 60) {
					sumMinute -= 60;
					sumHour++;
				}
			}

		}

		if (sumHour < 10 && sumMinute >= 10) {
			totalActualTime = "0" + sumHour + ":" + sumMinute + ":" + "00";

		}

		else if (sumHour >= 10 && sumMinute < 10) {
			totalActualTime = sumHour + ":0" + sumMinute + ":" + "00";

		}

		else if (sumHour < 10 && sumMinute < 10) {
			totalActualTime = "0" + sumHour + ":0" + sumMinute + ":" + "00";

		} else {
			totalActualTime = sumHour + ":" + sumMinute + ":" + "00";

		}

		return totalActualTime;

	}

	public static int buildHour(String stringActualTime) {
		StringBuilder buildHour = new StringBuilder();
		String[] timeArray = stringActualTime.split("");
		for (int i = 0; i < 2; i++) {
			buildHour.append(timeArray[i]);
		}
		stringActualTime = new String(buildHour);
		int hour = Integer.parseInt(stringActualTime);

		return hour;
	}

	public static int buildMinute(String stringActualTime) {
		StringBuilder buildMinute = new StringBuilder();
		String[] timeArray = stringActualTime.split("");
		for (int i = 3; i < 5; i++) {
			buildMinute.append(timeArray[i]);
		}
		stringActualTime = new String(buildMinute);
		int minute = Integer.parseInt(stringActualTime);
		return minute;
	}
}
