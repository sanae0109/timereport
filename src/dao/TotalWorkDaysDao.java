package dao;

import java.sql.SQLException;
import java.util.List;

import dto.TimeReport;

public class TotalWorkDaysDao {

	public static int TotalWorkDays(List<TimeReport> timeList2) throws ClassNotFoundException, SQLException {

		int total = 0;


		for (int i = 0; i < timeList2.size(); i++) {
			if (timeList2.get(i).getStartTime() != null && timeList2.get(i).getFinishTime() != null) {
				total++;

				}
			}

		return total;
		}

	}

