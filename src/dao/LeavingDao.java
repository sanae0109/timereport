package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dto.TimeReport;

public class LeavingDao {
	public boolean canLeaving(TimeReport timeReport) throws ClassNotFoundException, SQLException {

		Connection connection = null;

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance", "fukutarou",
					"fukutarou");

			String sql = "update time_report set finish_time = ? where user_id = ? and working_date = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setTime(1, timeReport.getFinishTime());
			preparedStatement.setString(2, timeReport.getUserId());
			preparedStatement.setDate(3, timeReport.getWorkingDate());

			int result = preparedStatement.executeUpdate();

			if (result != 1) {
				System.out.println("updateできんでした。。。。。。");
				return false;
			}

			//休憩時間の入力
			StringBuilder buildSql = new StringBuilder();
			buildSql.append("update time_report set break_time = ");
			buildSql.append("case when finish_time - start_time <='1:00' then CAST( '0:00' AS time ) ");
			buildSql.append("else CAST( '1:00' AS time ) end ");
			buildSql.append("where user_id = ? and working_date = ? ");

			sql = new String(buildSql);

			preparedStatement = connection.prepareStatement(sql);

			buildSql.delete(0, buildSql.length());

			preparedStatement.setString(1, timeReport.getUserId());
			preparedStatement.setDate(2, timeReport.getWorkingDate());

			result = preparedStatement.executeUpdate();

			//実働時間の入力
			sql = "update time_report set actual_time = ( finish_time - start_time - break_time ) where user_id = ? and working_date = ? ";
			preparedStatement = connection.prepareStatement(sql);

			preparedStatement.setString(1, timeReport.getUserId());
			preparedStatement.setDate(2, timeReport.getWorkingDate());

			result = preparedStatement.executeUpdate();

			//残業時間の計算
			buildSql.append("update time_report set over_time = ");
			buildSql.append("(case when actual_time <= '8:00' then CAST( '8:00' AS time ) ");
			buildSql.append("else actual_time end - '8:00' ) ");
			buildSql.append("where user_id = ? and working_date = ? ");

			sql = new String(buildSql);
			preparedStatement = connection.prepareStatement(sql);
			buildSql.delete(0, buildSql.length());

			preparedStatement.setString(1, timeReport.getUserId());
			preparedStatement.setDate(2, timeReport.getWorkingDate());

			result = preparedStatement.executeUpdate();

		} finally {
			if (connection != null) {
				connection.close();
			}
		}
		return true; // 退勤OK
	}

}
