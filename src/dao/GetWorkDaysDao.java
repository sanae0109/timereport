package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dto.TimeReport;

public class GetWorkDaysDao {

	public static List<TimeReport> GetWorkDays(String userId, int b, int i, int a)
			throws ClassNotFoundException, SQLException {

		Connection connection = null;
		TimeReport timeReport = null;

		List<TimeReport> timeList = new ArrayList<TimeReport>();

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance", "fukutarou",
					"fukutarou");

			for (int c = 1; c <= b; c++) {

				Date workingDate = null;
				Time startTime = null;
				Time finishTime = null;
				Time breakTime = null;
				Time actualTime = null;
				Time overTime = null;
				String location =null;
				String content = null;

				String stringDate = String.valueOf(i) + "-" + String.valueOf(a) + "-" + String.valueOf(c);
				Date date = Date.valueOf(stringDate);
				String sql = "select working_date,start_time,finish_time,break_time,actual_time,over_time,location,content from time_report where user_id = ? and working_date = ? ";
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, userId);
				preparedStatement.setDate(2, date);

				ResultSet resultSet = preparedStatement.executeQuery();

				// 一致したユーザが取得できれば
				while (resultSet.next()) {

					workingDate = resultSet.getDate("working_date");

					startTime = resultSet.getTime("start_time");
					if (startTime == null) {
						startTime = null;
					}

					finishTime = resultSet.getTime("finish_time");
					if (finishTime == null) {
						finishTime = null;
					}
					breakTime = resultSet.getTime("break_time");
					if (breakTime == null) {
						breakTime = null;
					}
					actualTime = resultSet.getTime("actual_time");
					if (actualTime == null) {
						actualTime = null;
					}
					overTime = resultSet.getTime("over_time");
					if (overTime == null) {
						overTime = null;
					}
					location = resultSet.getString("location");
					content = resultSet.getString("content");



				}

				if (workingDate == null) {
					workingDate = date;
				}

				Calendar calendar = Calendar.getInstance();

				calendar.set(buildYear(workingDate), buildMonth(workingDate), buildDay(workingDate));

				String dayOfWeek = null;

				switch (calendar.get(Calendar.DAY_OF_WEEK)) {

				case Calendar.SUNDAY: // Calendar.SUNDAY:1 （値。意味はない）
					dayOfWeek = "日";
					//日曜日
					break;
				case Calendar.MONDAY: // Calendar.MONDAY:2
					dayOfWeek = "月";

					//月曜日
					break;
				case Calendar.TUESDAY: // Calendar.TUESDAY:3
					dayOfWeek = "火";

					//火曜日
					break;
				case Calendar.WEDNESDAY: // Calendar.WEDNESDAY:4
					dayOfWeek = "水";

					//水曜日
					break;
				case Calendar.THURSDAY: // Calendar.THURSDAY:5
					dayOfWeek = "木";

					//木曜日
					break;
				case Calendar.FRIDAY: // Calendar.FRIDAY:6
					dayOfWeek = "金";

					//金曜日
					break;
				case Calendar.SATURDAY: // Calendar.SATURDAY:7
					dayOfWeek = "土";

					//土曜日
					break;
				}

				timeReport = new TimeReport(workingDate, startTime, finishTime, breakTime, actualTime, overTime,
						dayOfWeek, location, content);

				timeList.add(timeReport);

			}

		} finally {
			if (connection != null) {
				connection.close();
			}
		}


		return timeList;
	}

	//年を分けるメソッド
	public static int buildYear(Date workingDate) {
		StringBuilder buildYear = new StringBuilder();
		String stringWorkingDate = String.valueOf(workingDate);
		System.out.println(workingDate);
		String[] monthArray = stringWorkingDate.split("");
		for (int i = 0; i < 4; i++) {
			buildYear.append(monthArray[i]);
		}
		stringWorkingDate = new String(buildYear);
		System.out.println(stringWorkingDate);
		int year = Integer.parseInt(stringWorkingDate);
		return year;
	}

	//月を分けるメソッド
	public static int buildMonth(Date workingDate) {
		StringBuilder buildMonth = new StringBuilder();
		String stringWorkingDate = String.valueOf(workingDate);
		String[] monthArray = stringWorkingDate.split("");
		for (int i = 5; i < 7; i++) {
			buildMonth.append(monthArray[i]);
		}
		stringWorkingDate = new String(buildMonth);
		int month = Integer.parseInt(stringWorkingDate);
		month -= 1;
		return month;
	}

	//日を分けるメソッド
	public static int buildDay(Date workingDate) {
		StringBuilder buildDay = new StringBuilder();
		String stringWorkingDate = String.valueOf(workingDate);
		String[] dayArray = stringWorkingDate.split("");
		for (int i = 8; i < 10; i++) {
			buildDay.append(dayArray[i]);
		}
		stringWorkingDate = new String(buildDay);
		int day = Integer.parseInt(stringWorkingDate);
		return day;
	}

}
