package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.UserInfo;

public class AllAcountSelectDao {

	public static List<UserInfo>  AllAcountSelect() throws SQLException, ClassNotFoundException{

		Connection connection = null;
		List<UserInfo> accountList = new ArrayList<UserInfo>();

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance", "fukutarou",
					"fukutarou");

			String sql = "select user_id,user_name from mst_user_info";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);

			ResultSet resultSet = preparedStatement.executeQuery();

			while(resultSet.next()) {

				UserInfo userInfo = new UserInfo();



				Integer intUserId = resultSet.getInt("user_id");
				String userId = String.valueOf(intUserId);
				String userName = resultSet.getString("user_name");
				userInfo.setUserId(userId);
				userInfo.setUserName(userName);

				accountList.add(userInfo);




			}


		} finally {
			if (connection != null) {
				connection.close();
			}
		}
		return accountList;
		}

}
