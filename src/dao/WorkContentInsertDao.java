package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dto.TimeReport;

public class WorkContentInsertDao {

	Connection connection = null;

	public void workContentInsert(TimeReport timeReport, String userId) throws ClassNotFoundException, SQLException {

		Date today = new Date(System.currentTimeMillis());


		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance", "fukutarou",
					"fukutarou");

			String sql = "update time_report set location = ? , content= ? where user_id = ? and working_date = ? ";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);

			System.out.println(timeReport.getLocation());
			System.out.println(timeReport.getContent());
			System.out.println(userId);
			System.out.println(today);



			preparedStatement.setString(1, timeReport.getLocation());
			preparedStatement.setString(2, timeReport.getContent());
			preparedStatement.setString(3, userId);
			preparedStatement.setDate(4, today);



			int result = preparedStatement.executeUpdate();

		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}
}
