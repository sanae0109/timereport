package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dto.TimeReport;

public class BreakTimeDao {

		public boolean BreakTime (TimeReport timeReport) throws ClassNotFoundException, SQLException{

			Connection connection =null;

			try {
				Class.forName("org.postgresql.Driver");
				connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance","fukutarou","fukutarou");


				String sql = "update time_report set break_time = 1:00 where user_id = ? and working_date = ?";
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setTime(1,timeReport.getFinishTime());
				preparedStatement.setString(2,timeReport.getUserId());
				preparedStatement.setDate(3,timeReport.getWorkingDate());

				int result = preparedStatement.executeUpdate();

				if(result != 1) {
					System.out.println("updateできんでした。。。。。。");
					return false;
				}



			} finally {
				if (connection != null) {
					connection.close();
				}
			}
			return true; // 退勤OK
		}

	}


