package dao;

import java.sql.SQLException;
import java.util.List;

import dto.TimeReport;

public class TotalOverTimeDao {
	public static String TotalOverTime(List<TimeReport> timeList2) throws ClassNotFoundException, SQLException {

		String totalOverTime = null;
		int sumHour = 0;
		int sumMinute = 0;

		for (int i = 0; i < timeList2.size(); i++) {
			if (timeList2.get(i).getActualTime() != null) {
				String stringOverTime = String.valueOf(timeList2.get(i).getOverTime());
				System.out.println(stringOverTime);
				int hour = buildHour(stringOverTime);
				int minute = buildMinute(stringOverTime);
				sumHour += hour;
				sumMinute += minute;
				if (sumMinute >= 60) {
					sumMinute -= 60;
					sumHour++;
				}
			}

		}

		if (sumHour < 10 && sumMinute >= 10) {
			totalOverTime = "0" + sumHour + ":" + sumMinute + ":" + "00";

		}

		else if (sumHour >= 10 && sumMinute < 10) {
			totalOverTime = sumHour + ":0" + sumMinute + ":" + "00";

		}

		else if (sumHour < 10 && sumMinute < 10) {
			totalOverTime = "0" + sumHour + ":0" + sumMinute + ":" + "00";

		} else {
			totalOverTime = sumHour + ":" + sumMinute + ":" + "00";

		}

		return totalOverTime;

	}

	public static int buildHour(String stringOverTime) {
		StringBuilder buildHour = new StringBuilder();
		String[] timeArray = stringOverTime.split("");
		for (int i = 0; i < 2; i++) {
			buildHour.append(timeArray[i]);
		}
		stringOverTime = new String(buildHour);
		int hour = Integer.parseInt(stringOverTime);

		return hour;
	}

	public static int buildMinute(String stringOverTime) {
		StringBuilder buildMinute = new StringBuilder();
		String[] timeArray = stringOverTime.split("");
		for (int i = 3; i < 5; i++) {
			buildMinute.append(timeArray[i]);
		}
		stringOverTime = new String(buildMinute);
		int minute = Integer.parseInt(stringOverTime);
		return minute;
	}

}
