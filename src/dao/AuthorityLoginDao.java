package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.Authority;

public class AuthorityLoginDao {

public	Authority AuthorityLogin(String name) throws ClassNotFoundException, SQLException {

		Connection connection =null;
		Authority authority = null;

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance","fukutarou","fukutarou");

		String sql = "select authority_name from public.mst_authority where authority_name=?";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setString(1,name);

		ResultSet resultSet = preparedStatement.executeQuery();

		// 一致したユーザが取得できれば
					if (resultSet.next()) {


						String authorityName = resultSet.getString("authority_name");

						 authority= new Authority(authorityName);



					}

		}finally {
			if(connection != null) {
				connection.close();
			}

		}
		return authority;


	}

}
