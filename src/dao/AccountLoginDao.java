package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.Login;
import dto.UserInfo;

public class AccountLoginDao{

public UserInfo Login(Login login) throws ClassNotFoundException, SQLException {

	Connection connection =null;
	UserInfo userInfo = null;

	try {
		Class.forName("org.postgresql.Driver");
		connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance","fukutarou","fukutarou");

	String sql = "SELECT user_id,mail_address,password,user_name FROM public.mst_user_info WHERE mail_address=? AND password=?";
	PreparedStatement preparedStatement = connection.prepareStatement(sql);
	preparedStatement.setString(1,login.getMailAddress());
	preparedStatement.setString(2,login.getPassword());

	ResultSet resultSet = preparedStatement.executeQuery();

	// 一致したユーザが取得できれば
				if (resultSet.next()) {

					String userId = resultSet.getString("user_id");
					String mailAddress = resultSet.getString("mail_address");
					String password = resultSet.getString("password");
					String userName = resultSet.getString("user_name");

					userInfo = new UserInfo(userId,mailAddress, password,userName );



				}

	}finally {
		if(connection != null) {
			connection.close();
		}

	}
	return userInfo;


}

}
