package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.TimeReport;
import dto.UserInfo;

public class AdmissionDao {

	public boolean canAdmission (UserInfo userInfo,TimeReport timeReport) throws ClassNotFoundException, SQLException{

		Connection connection =null;

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1/attendance","fukutarou","fukutarou");

			String sql = "SELECT start_time FROM public.time_report WHERE user_id=? AND working_date=?";
			System.out.println(timeReport.getUserId());
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, timeReport.getUserId());
			preparedStatement.setDate(2, timeReport.getWorkingDate());

			ResultSet rs = preparedStatement.executeQuery();

			if (rs.next()) {
				System.out.println("出社が被っています。エラーです");
		  		return false; // 出勤が被っています。OUT

			}

			else {
				String sql1 = "INSERT INTO time_report(user_id,working_date,start_time) VALUES(?,?,?)";

				PreparedStatement preparedStatement1 = connection.prepareStatement(sql1);
				preparedStatement1.setString(1, userInfo.getUserId());
				preparedStatement1.setDate(2, timeReport.getWorkingDate());
				preparedStatement1.setTime(3, timeReport.getStartTime());

				int result = preparedStatement1.executeUpdate();

				// もし一件もINSERTできなければ
				if (result != 1) {
					System.out.println("INSERTできませんでした。エラーです");
					return false;
				}
			}
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
		return true; // 出勤OK
	}

	}
