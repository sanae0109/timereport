package dto;

public class Authority {

private String authorityName;  //管理者名

public Authority(String authorityName) {
	this.authorityName = authorityName;
}


public String getAuthorityName() {
	return authorityName;
}

public void setAuthorityName(String authorityName) {
	this.authorityName = authorityName;
}



}
