package dto;

import java.sql.Date;
import java.sql.Time;

public class TimeReport {

	private String timeReportId;         // ID
	private String userId;               // ユーザーID
	private Date workingDate;          // 出勤日
	private String dateTypeId;           // 日程種別ID
	private Time startTime;            // 出社時間
	private Time finishTime;           // 退社時間
	private Time breakTime;            // 休憩時間
	private Time overTime;             // 残業時間
	private Time actualTime;           // 実働時間
	private String projectId1;           // プロジェクトID1
	private String projectActualTime1;   // プロジェクト実働時間1
	private String projectId2;           // プロジェクトID2
	private String projectActualTime2;   // プロジェクト実働時間2
	private String projectId3;           // プロジェクトID3
	private String projectActualTime3;   // プロジェクト実働時間3
	private String location;             // 作業場所
	private String content;              // 作業内容
	private String memo;                 // 備考
	private Date registDate;           // 登録日
	private Date updateDate;           // 更新日
	private String dayOfWeek;          // 曜日 dbにはなし


	public TimeReport(String timeReportId, String userId, Date workingDate, String dateTypeId, Time startTime, Time finishTime, Time breakTime, Time overTime, Time actualTime, String projectId1, String projectActualTime1, String projectId2, String projectActualTime2, String projectId3, String projectActualTime3, Date registDate, Date updateDate) {
		this.timeReportId = timeReportId;
		this.userId = userId;
		this.workingDate = workingDate;
		this.dateTypeId = dateTypeId;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.breakTime = breakTime;
		this.overTime = overTime;
		this.actualTime = actualTime;
		this.projectId1 = projectId1;
		this.projectActualTime1 = projectActualTime1;
		this.projectId2 = projectId2;
		this.projectActualTime2 = projectActualTime2;
		this.projectId3 = projectId3;
		this.projectActualTime3 = projectActualTime3;
		this.registDate = registDate;
		this.updateDate = updateDate;
	}

	public TimeReport(String userId , Date workingDate , Time startTime , Time finishTime) {
		this.userId = userId;
		this.workingDate = workingDate;
		this.startTime = startTime;
		this.finishTime = finishTime;
	}

	public TimeReport(String userId , Date workingDate , Time finishTime) {
		this.userId = userId;
		this.workingDate = workingDate;
		this.finishTime = finishTime;
	}

	public TimeReport(Date workingDate,Time startTime , Time finishTime , Time breakTime , Time actualTime , Time overTime , String dayOfWeek, String location,String content) {
		this.workingDate = workingDate;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.breakTime = breakTime;
		this.actualTime = actualTime;
		this.overTime = overTime;
		this.dayOfWeek = dayOfWeek;
		this.location = location;
		this.content = content;
	}

	public TimeReport(Date workingDate,Time startTime , Time finishTime , Time breakTime , Time actualTime , Time overTime) {
		this.workingDate = workingDate;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.breakTime = breakTime;
		this.actualTime = actualTime;
		this.overTime = overTime;
	}

	public TimeReport(Time actualTime , Date workingDate) {
		this.actualTime = actualTime;
		this.workingDate = workingDate;
	}


	public TimeReport (String location , String content) {
		this.location = location;
		this.content = content;
	}



	public String getTimeReportId() {
		return timeReportId;
	}


	public void setTimeReportId(String timeReportId) {
		this.timeReportId = timeReportId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public Date getWorkingDate() {
		return workingDate;
	}


	public void setWorkingDate(Date workingDate) {
		this.workingDate = workingDate;
	}


	public String getDateTypeId() {
		return dateTypeId;
	}


	public void setDateTypeId(String dateTypeId) {
		this.dateTypeId = dateTypeId;
	}


	public Time getStartTime() {
		return startTime;
	}


	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}


	public Time getFinishTime() {
		return finishTime;
	}


	public void setFinishTime(Time finishTime) {
		this.finishTime = finishTime;
	}


	public Time getBreakTime() {
		return breakTime;
	}


	public void setBreakTime(Time breakTime) {
		this.breakTime = breakTime;
	}


	public Time getOverTime() {
		return overTime;
	}


	public void setOverTime(Time overTime) {
		this.overTime = overTime;
	}


	public Time getActualTime() {
		return actualTime;
	}


	public void setActualTime(Time actualTime) {
		this.actualTime = actualTime;
	}


	public String getProjectId1() {
		return projectId1;
	}


	public void setProjectId1(String projectId1) {
		this.projectId1 = projectId1;
	}


	public String getProjectActualTime1() {
		return projectActualTime1;
	}


	public void setProjectActualTime1(String projectActualTime1) {
		this.projectActualTime1 = projectActualTime1;
	}


	public String getProjectId2() {
		return projectId2;
	}


	public void setProjectId2(String projectId2) {
		this.projectId2 = projectId2;
	}


	public String getProjectActualTime2() {
		return projectActualTime2;
	}


	public void setProjectActualTime2(String projectActualTime2) {
		this.projectActualTime2 = projectActualTime2;
	}


	public String getProjectId3() {
		return projectId3;
	}


	public void setProjectId3(String projectId3) {
		this.projectId3 = projectId3;
	}


	public String getProjectActualTime3() {
		return projectActualTime3;
	}


	public void setProjectActualTime3(String projectActualTime3) {
		this.projectActualTime3 = projectActualTime3;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getMemo() {
		return memo;
	}


	public void setMemo(String memo) {
		this.memo = memo;
	}


	public Date getRegistDate() {
		return registDate;
	}


	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}




}
