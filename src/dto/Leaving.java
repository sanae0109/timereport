package dto;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import dao.LeavingDao;

public class Leaving {
	//退社
		public boolean isExecute(String userId , Date today) throws ClassNotFoundException, SQLException, ParseException{

			// 現在日時、時刻を取得
					SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");

					Date workingDay = today;

					String nowTime = simpleDateFormat1.format(today);

					Time finishTime = new Time(new SimpleDateFormat("HH:mm").parse(nowTime).getTime());
					System.out.println(workingDay);
					System.out.println(finishTime);

					TimeReport timeReport = new TimeReport(userId, workingDay, finishTime);

					LeavingDao leavingDao = new LeavingDao();
					return leavingDao.canLeaving(timeReport);
		}
}
