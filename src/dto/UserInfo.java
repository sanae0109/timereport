package dto;

public class UserInfo {

	private String userId;        //ユーザーID
	private String mailAddress;   //メールアドレス
	private String password;      //パスワード
	private String userName;      //ユーザー名
	private String departmentId;  //部署ID
	private String authorityId;   //管理者ID
	private String registDate;    //登録日
	private String updateDate;    //更新日


	public UserInfo(String userId,String mailAddress,String password,String userName, String departmentId, String authorityId, String registDate, String updateDate) {
		this.userId = userId;
		this.mailAddress = mailAddress;
		this.password = password;
		this.userName = userName;
		this.departmentId = departmentId;
		this.authorityId = authorityId;
		this.registDate = registDate;
		this.updateDate = updateDate;
	}

	public UserInfo() {}

	public UserInfo(String userId,String mailAddress,String password,String userName) {

		    this.userId = userId;
			this.mailAddress = mailAddress;
			this.password = password;
			this.userName = userName;
	}

	public UserInfo(String userName,String mailAddress,String password) {

		this.userName = userName;
		this.mailAddress = mailAddress;
		this.password = password;
}


	public UserInfo(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getAuthorityId() {
		return authorityId;
	}

	public void setAuthorityId(String authorityId) {
		this.authorityId = authorityId;
	}

	public String getRegistDate() {
		return registDate;
	}

	public void setRegistDate(String registDate) {
		this.registDate = registDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


}
