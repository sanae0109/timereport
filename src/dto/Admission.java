package dto;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import dao.AdmissionDao;

public class Admission {

	//出社
	public boolean isExecute(String userId , Date today) throws ClassNotFoundException, SQLException, ParseException{

		// 現在日時、時刻を取得
				SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");

				Date workingDay = today;

				String nowTime = simpleDateFormat1.format(today);
				Time startTime = new Time(new SimpleDateFormat("HH:mm").parse(nowTime).getTime());


				Time finishTime = new Time(new SimpleDateFormat("HH:mm").parse(nowTime).getTime());
				System.out.println(workingDay);
				System.out.println(startTime);
				System.out.println(finishTime);

				UserInfo userInfo = new UserInfo(userId);
				TimeReport timeReport = new TimeReport(userId,workingDay, startTime,finishTime);



				AdmissionDao admissionDao = new AdmissionDao();
				return admissionDao.canAdmission(userInfo,timeReport);
	}

}
