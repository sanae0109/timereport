package dto;

public class DepartmentInfo {

	private String departmentId;      //部署ID
	private String departmentName;    //部署名

	public DepartmentInfo(String departmentId , String departmentName){
		this.departmentId = departmentId;
		this.departmentName = departmentName;
	}



	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}



}
